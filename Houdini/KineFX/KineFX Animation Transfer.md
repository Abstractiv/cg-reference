![[kinefx_animtransferexample.png]]

In order to transfer the animation from one rig to another, you should follow the steps found at this link: https://www.sidefx.com/docs/houdini/character/kinefx/retargeting.html

You'll want to make one change though. You'll want to run the rest position of the animated rig you're transferring animation from through the Rig Match Pose to generate the rest_transform attribute from that pose. Then you want to transfer that generated rest_transfer to the animated version of the rig. After that you can set everything else up as described at the above link.