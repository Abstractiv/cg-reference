---
title: "Matte Proxies"
categories:
- Houdini
tags:
- Redshift Houdini
---

There's a 50/50 chance your proxies will render correctly if set as a Matte object the same way as anything else. If you end up on the unlucky side of these odds, you'll want to go to the Redshift OBJ tab on the Geometry, go to the Proxy tab, and check Override Visibility & Matte

![[RS_Matte_Proxies.png]]