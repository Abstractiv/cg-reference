---
title: "Hair Rendering"
categories:
- Houdini
tags:
- Redshift Houdini
---

### RS Hair vs RS Principled Hair
There are two different hair shaders in Redshift, RS Hair and RS Principled Hair. 
RS Principled Hair is the newer node, and features a more physically-based workflow providing sliders for aspects such as melanin content and different kinds of glossiness that are relevant to hair rendering.

### Ramped Coloration
If you want to have a color gradient from the root to the tip of the hair, in Redshift you ought to make use of the RS Hair Position node. This onde outputs a curve UV that you can throw into an RS Ramp that you can throw into a hair shader color input. The left end of the ramp in this case would correspond to the root color, and the right end would correspond to the tip color.