## Failed to load Redshift preferences file
Every once in a while, Redshift will stop working with TOPs. It starts complaining about
```
[Redshift] RAISING EXCEPTION: Failed to load Redshift preferences file '/home/hquser/redshift/preferences.xml'. Reverting to default preferences.
```
This happens when the file becomes corrupted. If you open the file, usually what happened is a couple of lines got duplicated at the end, and partially. If you just delete the duplicated lines it should start working again.