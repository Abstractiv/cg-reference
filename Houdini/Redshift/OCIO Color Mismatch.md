---
title: "OCIO Color Mismatch"
categories:
- Houdini
tags:
- Redshift Houdini
---

Sometimes you'll have discrepancy between what you see in the renderview versus what actually gets rendered out to disk, at least on Linux.

One potential cause of this is that you need to configure your OCIO [[Environment Variables|environment variable]] at a system level. If you just set the OCIO variable inside of the houdini.env, you'll get correct display in renderviews, but in my experience, the colorspace when saving to disk will be incorrect.

Another issue I've run into before is that the OCIO variables are working correctly, however the Color Space parameter on the RS Texture nodes are set incorrectly. If you're downloading an asset from online, for whatever reason the Color Space parameter might be auto-filled in as ACES2065. The renderview might render that texture in ACES2065, but when rendering to disk, it may question that decision and decide to auto-convert it to sRGB, which in most cases is correct. The solution here is to just double check that parameter and make sure it is either set to Auto, or manually set to whatever you know it should be. For diffuse textures, that should be Input Generic sRGB or whatever.