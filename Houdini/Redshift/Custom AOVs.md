---
title: Custom AOVs
categories:
- Houdini
tags:
- Redshift Houdini
---
Custom AOVs are fairly simple to set up. If you are working in Solaris, see the Solaris [[AOVs#Custom AOVs|notes]].