---
title: TOPs
categories:
- Houdini
tags:
- Redshift Houdini
---

When rendering in TOPs using the Local Scheduler, you'll want to make a couple small changes to your scheduler TOP. First, you'll want to change to change the total slots to "Equal to CPU Count Less One", and second, you'll want to go to the Job Parms tab and check "Single". Basically, you don't want to run multiple renders simultaneously on the same machine.

When rendering in TOPs using the Hqueue Scheduler, in order to restrict to one render per machine, under the Job Parms tab, you'll want to add `single` to the **Tags** parameter. See [[Hqueue Scheduler#Running single sim or render per machine on hqueue scheduler|here]] for more information.