---
title: "Miscellaneous"
categories:
- Houdini
tags:
- Redshift Houdini
---

###  Photometric Units to Meter Scale
This is a setting on the Redshift ROP under Redshift -> Advanced -> System that tells the renderer what the default units in your scene are. A value of 100 (the default) means that it takes 100 scene units to make a meter, i.e. your scene units are in centimeters. If you are in Houdini, the default scene scale is in meters, so you should adjust this value to be 1.

This setting is only relevant if you are using photographic exposure, the physical sun, or IES lights.