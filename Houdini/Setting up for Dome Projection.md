Rendering in Karma, you'll want to create a Karma Physical Lens shader in a material network (not a material library, as it's not a pure VOPs space). Under Lens Distortion, you'll want to switch projection to Polar.

![[physical_lens_projection.png]]

On second thought, this won't work the way I want.