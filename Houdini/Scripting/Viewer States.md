	wYou can create new viewer states from either the Interactive tab of the Type Properties of your HDA, or directly from the Viewer States Browser under File > New after selecting a context.

A good practice would be to name your state for the asset it's attached to if being generated from an HDA.

Nodeless States are loaded from scripts found in the paths defined in the HOUDINI_VIEWERSTATES_PATH environment variable.
To use a viewer state defined by a py file on an HDA, supply the name of the viewer state in the Default State parameter under the Node tab of Type Properties.

## Event Handlers
Doc: sidefx.com/docs/houdini/hom/python_states.html
What is kwargs? Kwargs is a dictionary containing useful information regarding the tool, including the node definition, any menu items, state parameters, and state flags.

## Geometry Intersection
I want to click and add points on the surface of geometry. Using the built-in add point template only lets you add points to the reference grid, so we need to make something a bit more custom. We'll want to use the hou.Geometry.intersect method to get the ray intersection with the geometry. The documentation recommends wrapping this method in a utility function, for example (as taken from the docs): 
```python
# In viewerstate.utils
def sopGeometryIntersection(geometry, ray_origin, ray_dir):
    # Make objects for the intersect() method to modify
    position = hou.Vector3()
    normal = hou.Vector3()
    uvw = hou.Vector3()
    # Try intersecting the ray with the geometry
    intersected = geometry.intersect(
        ray_origin, ray_dir, position, normal, uvw
    )
    # Returns a tuple of four values:
    # - the primitive number of the primitive hit, or -1 if the ray didn't hit
    # - the 3D position of the intersection point (as Vector3)
    # - the normal of the ray to the hit primitive (as Vector3)
    # - the uvw coordinates of the intersection on the primitive (as Vector3)
    return intersected, position, normal, uvw
```