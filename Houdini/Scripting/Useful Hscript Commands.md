### Finding References and Changing Paths

Imagine you have a file you brought from home, and you want to use it at work. Unfortunately, the file system is different at home than at work. You have hundreds of file paths to change, how do you do it?

Note that nothing is visible! This is because of bogus file references. Dive inside one of the objects. Look at the file path:

```bash
/E:/FX/COURSEWARE/MODELS/Chambord/Chambord.obj
```

Let's change "E:" to "LOSTBOYS". Open a texport and:

```bash
opchange E: /home/user
```

Note that you can also use "opfind" to find references to things. Now try:

```bash
opfind /home/user
```

Let's say you didn't know the full name, you just knew it had the word "LOST" in it:

```bash
opfind -i -w LOST*
```

This command is also very useful for finding channel references to another object. Let's see you want to see all objects that reference the node "chambord".

```bash
opfind chambord
```

It tells you that the object "Neuschwanstein" references "chambord".

**NOTE** that if you are trying to replace the word "$GEO" with the opchange command you have to "protect" the $ sign. This is handy when you go from work to home, and you are not using environment variables.

```bash
opchange \$GEO /home/melvin
```

## rand

Returns a pseudo random number between zero and one, (meaning it's not really random... next time you open houdini you will get the same "random" number.)

- `rand(@ptnum)` --> Give me a different random number for each point.
- `rand(opdigits("."))` --> Give me a different random number for each SOP.
## opdigits

Returns the digits in a operator name. Let's say we are looking at the sop /obj/foot2/ankle3 and we put an expression in the SOP "ankle2"

- `opdigits("."))` --> Give me the digits in the current SOP which is called "ankle3". This will return "3"
- `opdigits(".."))` --> Give me the digits in the current SOP's _parent_ which is called "foot2". This will return "2"
- `opdigits("../.."))` --> Give me the digits in the current SOP's _parent's parent_ which is called "obj". This will return "0"

## rand and opdigits

- Let's combine "rand" and "opdigits" to get a different random number for each object. If you were to put this in the "scale" paramater of a digital asset, each object would get a different random scale when you created it.

```bash
rand(opdigits("."))
```