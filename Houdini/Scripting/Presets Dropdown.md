---
title: Presets Dropdown
categories:
- Houdini
tags:
- Houdini
---
Here we'll set up a dropdown menu for picking a set of parameter presets.

First, you want to create a dropdown menu to select between your preset. In this example, I created a dropdown with three options, "Low", "Med", and "High". I named the parameter "presets".

![[presets_param.png]]

Next, you need to create a script on the Scripts tab. One way to make one is to create an empty file somewhere, then to put that in the Filename and hit "Add File". That will embed the contents of that empty file into a new script in the scripts tab. Give it a descriptive name, then look to the right side pane. Near the bottom should be a dropdown named "Edit as". Make sure this is set to Hscript.

![[presets_scriptlist.png]]

We can reference a script file on an HDA by using opdef:.?<name>. In this case, we want to set our script as the Callback Script on the ordered menu so that it gets called every time we make a new selection.

![[presets_callback.png]]

Now, let's actually set up the logic of our script. Go back to the "Scripts" tab, make sure our presets script is selected, and in the editor to the right we can start adding logic. There are three major steps we want to complete to implement our scripts.

1) Fetch the current selection in the presets menu.
2) Depending on the selection, set variables for each parameter we want update values for with our preset.
3) Actually set the parameter values using the variables we created.

Breaking down each step:
**STEP ONE**
We can fetch the current selection by using the following code:

```
set preset = `ch("presets")`
```
We use the `set` command in order to declare and set the value of a variable. 
We need to nest the `ch()` function, which reads the value of a parameter, in backticks because it is technically an Hscript Expression, not Hscript, and nesting in backticks tells the script to interpret what's inside as an expression as opposed to an Hscript command. 

**STEP TWO**
This is where we actually set the lists of values. We will use an if statement to set values depending on which preset is selected. 

```
if ( $preset == 0 ) then
    set myint = 1
    set myfloat = 1.0
    set myvec = 1.0 1.0 1.0
else if ( $preset == 1 )
    set myint = 5
    set myfloat = 5.0
    set myvec = 5.0 5.0 5.0
else if ( $preset == 2 )
    set myint = 15
    set myfloat = 15.0
    set myvec = 15.0 15.0 15.0
else
    set myint = 0
    set myfloat = 0.0
    set myvec = 10 11 12
endif
```

Note: We are not setting the actual parameters right now. These variables only exist in the context of the script right now. We actually set the parameters in the next step:

**STEP THREE**

The `opparm` command is used to get, set, and otherwise manipulate parameter values on nodes. The syntax for setting parameter values is as follows:

`opparm <node> <parameter> <value>`

```
opparm . my_int $myint
opparm . my_float $myfloat
opparm . my_vec $myvec
```
We used "." here are the address for our node. That just means "the current node".

Hit apply and accept, and if everything is done correctly, then you should be able to just flip to different options in the presets dropdown and your parameters should update accordingly.