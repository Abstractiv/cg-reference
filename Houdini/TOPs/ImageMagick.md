---
title: ImageMagick
categories:
  - Houdini
---
## Montage: Custom Number of Columns
Setting up a montage/contact sheet is easy enough, just stick the ImageMagick node down after a Wait for All on a bunch of renders and the defaults will create a nice contact sheet of everything. Only problem is it seems to default to six columns. In my particular case, I want 7 columns because I'm comparing three sets of seven renders. ImageMagick has a custom argument for this called tile. If we want are image to be built in rows of 7 tiles, we can provide said custom argument like so:

![[magick_tile.png]]

Now the montage is 7 columns wide:

![[magick_montageexample.png]]

## Convert Colorspace
Further reading: https://opencolorio.readthedocs.io/en/latest/tutorials/baking_luts.html

NOTE: THIS SEEMS TO HAVE STOPPED WORKING RECENTLY
You probably saved your render out as an EXR in ACEScg colorspace, however if you use imagemagick to convert to a format like jpeg or png you'll see that the colors get all messed up because imagemagick doesn't know the conversion necessary and the exr's aren't saved out with descriptive enough metadata. Unfortunately, imagemagick does not use OCIO for color management, it uses ICC profiles, so we'll need to create one for converting from acescg to srgb (you can probably find one online, but here's how to make your own if you can't find one). This was done one Ubuntu 22.04 with the ocio package installed. On Fedora, you must install the OpenColorIO-tools package.

````BASH
ociobakelut --format icc --inputspace acescg --outputspace "Output - sRGB" --displayicc /usr/share/color/icc/colord/sRGB.icc --description "acescg to srgb" $HOME/acescg_to_srgb.icc
````

Note: After installing ACES 1.3, the command changed to 
```bash
ociobakelut --format icc --inputspace acescg --outputspace "sRGB - Display" --displayicc /usr/share/color/icc/colord/sRGB.icc --description "acescg to srgb" $HOME/acescg_to_srgb.icc
```

You may need to adjust the displayicc if you're on another distro that saves its built-in icc profiles to a different location.

Now, in ImageMagick, you can specify the profile argument and point to the ICC profile you just created. 

![[magick_profileconvert.png]]

Now, when running the convert command from exr to png, it also converts from acescg to srgb.

Still having issues with jpg for some reason.

## Add Text Overlay
https://imagemagick.org/Usage/text/