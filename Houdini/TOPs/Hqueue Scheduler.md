---
title: Hqueue Scheduler
categories:
- Houdini TOPs
---
## Hqueue Scheduler
These are the settings to use for my particular setup. Ymmv on this one, depending on the specifics of the farm you're using.

## Scheduler Tab
Make sure the IP address to your hqueue server is set with the correct port, eg. http://192.168.1.10:5000

Edit the Job Name to something descriptive for your sake and others. This is the name that appears in the job list in the hqueue job interface.

Also make sure the HFS path is set correctly.This is just the install location of Houdini that's accessible to all your nodes of a given OS type. In my case, that install is located on a network folder, but this could also just be a common install path that's used on all your machines.

## Message Queue
This is important for the specific setup I'm using. The message queue is a service that runs in the background that monitors the various tasks and reports them back so that Houdini can update the little progress dots. By default, this is set to "Farm", and so it will put this little watcher process on one of the farm nodes. This is a problem for me as my simulation client group has only one machine. What ends up happening in this case is that the farm will put the message queue service on the one machine in the simulation group and then find any other random machine it can find to put the simulation on. It defeats the whole point of having this simulation group. 

As such, I set this to "Local". This will just put that process on your current machine. It's a small process so it's never an issue for me. 

![[tops_hqueue_messagequeue.png]]

## Job Parms
Make sure your client group is set correctly.

If you want to set it up so that each job uses one machine on the farm, you'll want to set the "single" tag. See [[#Running single sim or render per machine on hqueue scheduler]] for more details.

## Environment Variables
This one is annoying. If you're using the scheduler to render with Redshift, you need to make sure you have a bunch of environment variables set up correctly so that Redshift (and any other custom environment variables) can be found. In my case there are 16 different environment variables I need to export. I could fill these out one by one, or we could use Hscript to set all these parameters for us

---
## Extra Info
### Running single sim or render per machine on hqueue scheduler
The hqueue scheduler doesn't have an exact analog to the "Single" Scheduling checkbox on the Job Parms tab on the local scheduler. The closest you have is the **Tags** parameter under the Job Parms tab. If you add `single` as a tag, it will only assign one job per client.

Alternatively, you need to use the "CPUs per Job" control on the Job Parms tab of the hqueue scheduler. If you know each node in your hqueue network has 12 CPUs, then you'll want to set CPUs per job to 12. Any machines that have less than 12 CPUs available won't have a new job assigned to it.

This is useful especially for Redshift since RS is mainly run on the GPU, while TOPs looks for available CPUs to assign jobs. If you have multiple jobs submitted to the farm, odds are it will try to put multiple redshift jobs onto the same node which, at least in the setups I've used, is a recipe for failed jobs all around.

### Automating This Setup with HScript
I've set up a dedicated [[Automate Hqueue Scheduler Setup with HScript|page]] for this topic.
