---
title: Automate Hqueue Scheduler Setup with HScript
categories:
- Houdini TOPs
tags:
- Houdini TOPs
---
In [[Hqueue Scheduler]] I wrote about the various things I need to set and check in order to use the Hqueue scheduler.

While we could do some of this by using the "Set Permanent Defaults" option, it doesn't really help when it comes to multiparms such as the Environment Variables parameter. Plus, you may be in a situation where you want to set different values when you're using different client groups, eg. if you have different render and sim client groups.

You can create a custom script on disk in either HScript or Python that automatically fills out various parameters that couldn't otherwise be setup via adjusting the node defaults.

Since we're just doing parameter setting, let's do this in Hscript. We'll want to create a .cmd file inside of our houdini config directory. This could be something like /home/<user>/houdini<version>, or it could be your $HSITE directory. If the scripts subfolder doesn't exist, you'll have to make it. In side of the scripts folder, create a text file named something like init_hq.cmd. The .cmd extension just delineates the script as an Hscript script.

Before diving into the script, go into the "Edit Parameter Interface" window and create a button on the Hqueue Scheduler that will run the script when pressed. 

In the Callback Script field, put
```
init_hq.cmd `opfullpath(".")` ; 
```
Replace "init_hq.cmd" with the name of your script. We pass opfullpath(".") as an argument to your script so that we have access to the location of the hqueue scheduler node inside the script. For example, if our hqueue scheduler is located at /obj/geo1/topnet1/hqueuescheduler, then that is what opfullpath will return. This is important because the script lives outside the scene and doesn't have a reliable way of knowing what node to set parameters on without us telling it. We can reference the value of this argument by calling `$arg1` inside the script.

```
set c = 1

opparm $arg1 "hqueue_envname"${c}  ( REDSHIFTPATH )  "hqueue_envvalue"${c} ( $REDSHIFTPATH )
set c = `$c + 1`

opparm $arg1 ${vn}${c} ( redshift_LICENSE )  ${vv}${c} ( $redshift_LICENSE )
set c = `$c + 1`

opparm $arg1 ${vn}${c} ( REDSHIFT_COREDATAPATH )  ${vv}${c} ( $REDSHIFT_COREDATAPATH )
set c = `$c + 1`
```