---
title: Intro to Orientation
categories:
- Houdini
tags:
- Houdini
---
In this lesson, we will cover the basics of orienting copied objects. 

## What for?
Instancing shapes onto points is one of the more common operations you'll find yourself doing in a project. Some examples:

- Scattering elements on a terrain
- Replacing simulated particles with geometry
- Swapping out low-res simulation geometry with high-resolution render geometry

It is important to know how to control how objects are scaled, rotated, and translated when they're copied onto a point.

If you're orienting something in Houdini, odds are you'll be doing it using a vector.

## What is a vector?
A vector is something that records a direction and a length. In 3D space, a vector is comprised of three values, the x, y, and z components. A vector in 3D space can be represented by an arrow that points from the origin to the coordinate encoded by the three values of the vector. **rewrite this**

Some noteworthy examples of vectors in Houdini are the attributes @P, @N, and @v. The @P attribute records the vector that starts from the space origin and ends at the location of the given point. It is indistinguishable from the concept of a coordinate. The @N attribute by default records the direction pointing away from a surface. @N should generally be normalized, which is to say it should have a length of one. A practical and common use for @N is in shading. If your surface were perfectly smooth and glossy, a light ray hitting a spot would bounce off the surface at an angle mirrored off about the surface normal **add diagram**

Finally, @v records velocity. An easy way to understand velocity is to first consider something you are perhaps more familiar with, speed. Speed is the rate that something is moving through space. Speed tells you how fast something is moving, but it doesn't tell you what direction. Velocity tells you both. Remember how a vector can be represented by an arrow? If we represent the velocity vector with an arrow, the direction the arrow is pointed represents the direction of movement, and the length of the arrow is the speed. An object moving in a straight line represented by a vector would move from the beginning of the vector to the end in a given unit of time. **add diagram**

## Attributes
### Rotation
The most common attributes you'll likely use to set the rotation of an instanced object are @N and @up. 

@N corresponds with the forward (Z) direction of the object being instanced, and @up corresponds with the, well, up (Y) direction. 
You can copy to points that only have the @N vector defined, and while the objects you copy will be turned to face in the direction of the @N of the points they're being copied to, they may do so at a surprising rotation around that axis. Providing the @up vector locks that rotation to a single, specific orientation.

Other attributes that are used to set rotation are @orient and @rot. If present, the orient attribute supercedes any orientation provided by @N or @up, and the rot attribute holds any additional rotations to be applied after all other rotations. Both @orient and @rot are quaternions, and as such are outside the scope of this lesson.

Finally, if no other orienting attributes are present, @v can be used to orient instances. This is useful in the case of POP simulations where you want your instanced geo to constantly face in the direction of movement.

### Scaling
There are two attributes used to affect the size of an instanced piece of geo, @pscale and @scale. The pscale attribute is a uniform scale, which is to say that it is applied uniformly on each axis. This is comparable to sliding the Uniform Scale parameter on the transform node, and as such, it is a float attribute. 

By contrast, @scale is a vector, and each component of the vector codes for its respective axis, i.e. the x (first) component scales along the x-axis, the y (second) component along y, etc. This is comparable the the scale parameter on the transform node.