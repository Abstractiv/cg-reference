---
title: Booleans
categories:
- Houdini
tags:
- Houdini
---
If you are using cutting plans to boolean shatter a surface, and you want to keep the seams and nothing else for whatever reason, output the abseams group, dissolve non-selected, and check create curves for whatever boundary *double check*

If you are using Boolean Shatter and it just isn't cutting correctly, make sure that your cutting plane has enough geo resolution. If you use something with little to no internal polygons then it seems to struggle.