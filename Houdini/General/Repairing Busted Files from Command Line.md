---
title: Repairing Busted Files from Command Line
categories:
- Houdini
tags:
- Houdini Linux Scripting Hscript
---
## Using Hscript from the Terminal
You can use the hscript command in your terminal to use hscript commands to adjust your file. 
`hscript <filename>`

Once you're in the hscript interface, you can navigate your file as if it were a linux file system. You can cd into contexts and nodes and, for example, you could navigate to a node and bypass it using the [opset](https://www.sidefx.com/docs/houdini/commands/opset.html), using a command like `opset -b on <node>`.

Once you've made your changes you can use the [mwrite](https://www.sidefx.com/docs/houdini/commands/mwrite.html) command to save the file.

`mwrite <new file name>`

`mwrite -i <file name>` will increment the number in your file name, if you've got one.

**Rebuilding the Entire Scene with Hscript**
If you know which node is causing your file to crash on startup, the above bypass command will like work wonders, however, every once in a while you may end up with a file corruption that is not related to a specific node. You can use the following commands to save out the Hscript recipe to rebuild your entire scene, then import that into a clean file:

```
opscript -b -r * > <filename>.cmd

# From a new file:
source <filename>.cmd
```

### Further Reading
[Documentation page for Hscript Utility](https://www.sidefx.com/docs/houdini/ref/utils/hscript.html)
[Hscript Language Reference](https://www.sidefx.com/docs/houdini/commands/_guide)
