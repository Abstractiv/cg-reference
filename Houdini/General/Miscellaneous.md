---
title: Miscellaneous
categories:
- Houdini
tags:
- Houdini
---
### Compute Velocity
There are a couple different ways you can take a moving object without a v attribute and have Houdini calculate the movement of each point and write it out to v.

Ths simplest would be to use the Trail SOP, and to switch the `Result Type` to 'Computer Velocity'. This does exactly what we described previously and little else. You can scale the computed velocity, you can change whether it's analyzed by looking back in time, forward, or both, and you can also calculate acceleration and angular velocity. Simple and effective, but sometimes you want a little extra control

In that case, you want to use the Point Velocity SOP. Point Velocity lets you add additional curl noise on top of your calculated noise, it lets you calculate velocity relative to another object, and it lets you add on velocity in either a directional or conical profile.

---
### Create a Reference Copy
Sometimes you want to create a copy of a node with every parameter referenced to the original. One example of this is a Transform node you want to copy so that you can invert its transformation later. You can do this by right-clicking on the node, and going to Actions -> Create Reference Copy.

---
### Houdini 19 Dependencies
There are a few dependencies in Linux that are needed with Houdini 19.0 but not installed with the program.

If you are getting the following error when attempting to run hkey:
```
"Qt Error: This application failed to start because no Qt platform plugin could be initialized. Reinstalling the application may fix this problem. Available platform plugins are: eglfs, linuxfb, minimal, minimalegl, offscreen, vnc, wayland-egl, wayland, wayland-xcomposite-egl, wayland-xcomposite-glx, webgl, xcb. "
```
You need to install or reinstall libxcb-xinerama0 in Ubuntu.

---
### Save UV Layout
Sometimes you want to save out an image of the UV layout of a given piece of geo so that you can take that template into a different program like Photoshop for texturing. In order to save out said image,  you simply need to right click on the node you want to save from, go to `Save` -> `Texture UV to Image`

---
### Change Values every N Frames
```
floor(@Frame / n)
```

---
### Labs Transfer UV
The main problem with trying to transfer UVs with your regular attribute transfer is that you end up losing your seams, meaning that wherever there were clean cuts in the UVs there will now be great stretching as the UVs are now connected and must wrap around the UV space.

You could try to write some VEX to try to recreate the UV islands yourself, but luckily someone at SideFX realized this was an issue and created a transfer tool for UVs. Just hook it up the way you would have with the regular attribute transfer and you should be good to go. 

---
### Random time offset for parameter animation
Say you want to lerp between two positions, a rest position and P.  You've animated the blend between them, but now you want to add some random time offset per point to that animation. The ch() function can take a second input that lets you specify the time to sample a value from, in seconds. 

If you want to get an identical result to just using the standard chf("name"), you can use chf("name", @Time). To offset, you'll just add or subtract some value from @Time. For example:

```
float random_offset = rand(@ptnum) * 0.5;
float blend = chf("amount", @Time - random_offset);

v@P = lerp(v@rest, v@P, blend);
```

---
### Set External Editor
The menu item in Houdini to set external editor, you need to set the EDITOR environment variable instead. The value you should be the path to executable of your editor of choice, for example EDITOR="/usr/bin/code".