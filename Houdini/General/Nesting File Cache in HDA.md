---
title: Nesting File Cache in HDA
categories:
- Houdini
tags:
- Houdini
---
One way to nest a file cache inside of an HDA is to make a button on the HDA interface that presses the Save button on the file cache inside. The following Hscript snippet set as a callback function on the button will do the job:

`opparm -c <cache node path> execute`

If you get a permissions issue, the reason is likely that you need to go to the Node tab of the Type Properties of the HDA, and under Editable Nodes, you need to add the file cache.