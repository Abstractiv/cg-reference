---
title: User Interface
categories:
- Houdini
tags:
- Houdini
---
### Badges
Have you ever noticed the various little icons that sit next to nodes? Want to know what they mean? Hit `D` to go into the network view display options, and then go to the various "Badges" tabs to figure out what you're looking at.

![[badges.png]]
