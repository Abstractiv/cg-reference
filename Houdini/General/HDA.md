---
title: HDA
categories:
- Houdini
tags:
- Houdini
---

## "Hide/Inactive When" Logic
Normally when you want to use the "Hide/Inactive when" functionality in your interface, you set the rules using the following format
```
{  param > 1.0 }
```

But what if you want to use a logical operator such as "and" or "or"?
Well it's not a very intuitive syntax, but it goes as follows.

Putting multiple comparisons inside one set of curly brackets means you want to do an AND statement, and putting them into their own sets of curly brackets means you want to do an OR statement.

Example:
If param1 is greater than 1.0 AND param2 < 30.0:
```
{ param1 > 1.0  param2 < 30.0 }
```
if param1 is greater than 1.0 OR param2  < 30.0:
```
{ param1 > 1.0 } { param2 < 30.0 }
```