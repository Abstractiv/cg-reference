---
title: Useful Keybindings
categories:
- Houdini
tags:
- Houdini
---
## Navigation
**Open display menu**
Press **d** to open the display options of whatever panel you have your mouse cursor over.

**Move one level up**
Use the **u** key to move one level up. For example, using this shortcut inside of a subnet will take you up and out of the subnet.

## Node Organization
**Auto align and space**
If you select a bunch of nodes in a line, hold **a** and drag-left click your mouse over them, they'll all be lined up and properly spaced.

**Flip node inputs**
**Shift-r** flips node inputs.

Use colors and network boxes as needed.

**Shift-s** changes the node connector style