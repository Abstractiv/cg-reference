The relatively recent Houdini Ocean Procedural has been updated to work with any Solaris renderer. This doesn't work 100% of the box with Redshift. 

First, the Procedural creates the interior volume as geometry, which is not a problem for renderers like Karma that can render mesh geometry as uniform volumes, however, as far as I'm aware, Redshift doesn't support that. So you'll either want to convert that geo to a volume or just disable it. 

Second, a Configure Layer node needs to be added to the Redshift output HDA immediately after the Render Settings node. All you need to do is check on Render Settings and point to the generated Render Settings primitive.

![[ocean_configure_layer.png]]