---
title: Intro to Oceans
categories:
- Houdini
---
## The Ocean Spectrum
At the heart of any ocean system in Houdini is the Ocean Spectrum. The Ocean Spectrum creates a number of volumes that describe the motion of waves. When paired with an Ocean Evaluate, you can use the generated volumes to displace a piece of geometry. 

## Masking and Wave Instancing
There are a few different ways to mask an Ocean Spectrum. One of the most common reasons to do so is to prevent repeating patterns, as the Ocean Spectrum tiles at the scale set in the Grid Size.
### Using a Masking Volume
If you create a fog VDB named "mask" and plug it into the Ocean Spectrum's second input, then you can tell that region to essentially not have waves. You can adjust the strength of this effect by simply multiplying the value of @mask in a Volume Wrangle/VOP.
### Noise
There are options to Mask via noise on the Ocean Spectrum's "Mask" tab.

### Filtering
You can use the "Filter Above" and "Filter Below" parameters on the Amplitude tab of the spectrum to, for example, keep a high resolution spectrum but remove higher frequency noise, and vice versa.

### Wave Instancing
### Adjusting Wave parameters with attributes
You can use parameters on the instancing points to adjust the strengths of various parameters found on the "Wave Instancing" tab on the Ocean Spectrum. If you are going to use the attribute instead of the parameter, you need to remember to check off said attribute.
### Waves in Weird Directions
When using point instancing, waves will flow in the direction of the instance points N vector. The up vector can also be used to ensure correct orientation.