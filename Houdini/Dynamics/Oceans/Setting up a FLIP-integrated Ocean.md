---
title: FLIP-integrated Ocean
categories:
- Houdini
---
Once you've got your wave box set up, you can use the particle fluid surface node to mesh it, just like any other FLIP sim. On the Flattening section in that node, you can use the flattening to, well, flatten out the rim of the simulation. You can use different mask shapes, or mask based on attributes such as velocity. You want to use this flattened rim to extend the edges using the Extend Polygons Along Each Axis