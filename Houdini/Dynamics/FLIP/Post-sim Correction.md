---
title: Post-sim Correction
categories:
- Houdini FLIP
---
For whatever reason, you may have a region of under-density. You can select that area and use a point replicate to beef up the point count for surfacing if you need to.