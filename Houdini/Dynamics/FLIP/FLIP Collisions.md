---
title: FLIP Collisions
categories:
- Houdini FLIP
---
## Adding Velocity
[[Houdini/General/Miscellaneous#Compute Velocity|Having a velocity attribute]] on the colliding objects helps a lot. You can scale those velocities to get particular effects, for example if you scale the velocity of the collider by 10, the object will have a disproportionate impact to its real velocity.

## Using a VDB
VDB collisions are also very helpful 
Collision VDB resolution is tied to the resolution of the underlying FLIP voxel grid. If you want to add collision resolution independently, you can enable the Collision Separation value in the FLIP Object. It's generally better to tweak particle separation and grid scale in order to fix your collision issues, but there may be times where you generally like where your settings at, but the collision object is the odd thing out and you only want to edit the collision object quality. Edit that then.
