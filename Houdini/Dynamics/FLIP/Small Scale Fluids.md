## Splashes
Flip Object -> Recommended Time Scale: 0.1
- Will obviously slow down the sim, but it's helpful for getting all the details at high resolution at this scale, *especially* 
- Min - Max substeps: 4-5 not a bad place to be.

Shoot for a scene scale of 1 - 2 meters. 

When iterating, try to keep your simulation point count above 10000 points.

The more particles, the lower the surface tension to get the same effect.

Make sure to enable the id attribute, this will make retiming easier later.

Applying Particle Separation 

Reseeding -> 15-20 particles per voxel really brings out the details on the sheets and meniscus. Not only that, but increasing resolution this way changes the end look less than changing resolution via the particle separation. For a final production shot, numbers can get up to 40-50, with a Surface Oversamping of 3-4, which helps with reseeding flickering and smoothness. NOTE: This high particles per voxel thing is really mainly important if you're trying to capture sheets. If it's just droplets then it's whatever.

You can delete ugly or misbehaving tendrils and elements by going into the viewport selector, switching to select by 3d connected geometry, and changing the +3d thingy to @id. If you do this you'll want to cache afterwards because it's a bit heavy.

## Viscous Donut
Wiggle animation -> sin(@Frame * 20) * chf("../someparam") * rand(@Frame)
- That middle parameter is just to let you control the amplitude with a keyframe if you so chose.

Viscosity vs Density attribute
Obviously, viscosity affects the "thickness" of the fluid, the tendency of the fluid to flow in a cohesive mass. Varying viscosity is helpful for working on an effect like lava or something melting.
Density affects the weight of the fluid. A viscous fluid with a uniform viscosity but a variable density will behave in some ways like a fluid with variable viscosity. Areas of high density will be pulled by gravity more than areas of low density, so this is good for things that appear to be dripping or melting at different rates in different patches. A bit more subtle of an effect than variable viscosity.