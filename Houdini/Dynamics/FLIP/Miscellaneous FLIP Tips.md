---
title: Miscellaneous FLIP Tips
categories:
- Houdini FLIP
---

Using POP source instead of fluid source can be faster, especially if you're not continuously sourcing new fluids and consequently not needing a surface volume for the volume source.

If you're using rigidbodies with FLIP they must be unpacked. For fractured objects it may be worth using [[RBD Fractured Object|RBD Fractured Objects]].
