---
title: Leaking Fluid
categories:
- Houdini FLIP
---
Sometimes, especially when you're dealing with thin-walled containers, you run into situations where your FLIP particles will escape the container through the walls. This can be a difficult problem to troubleshoot, but there are some places you can start.

First, you'll want to check a few areas that don't have much impact on simulation performance. 
The first two are elaborated on more in-depth in [[FLIP Collisions]]
- Use VDB Collisions, and make sure "Fill Interior" is enabled on the VDB from Polygons node.
	- Make sure you have sufficient VDB resolution. Voxels should be smaller than your particle separation.
- Make sure your collider has a velocity attribute.
- On the solver, under the Volume Motion tab, change the Extrapolation Method to 'Fast-moving Colliders'
- Switch Collision Detection to "None" or "Move Outside".
- On the solver, under the Particle Motion tab, enable "Apply Particle Separation".

If none of these tips work, there are some settings you can change that will impact performance.
- Decrease the grid scale
	- Start at 1.5 and go down to 1 if necessary.
- Increase the substeps on the FLIP solver.