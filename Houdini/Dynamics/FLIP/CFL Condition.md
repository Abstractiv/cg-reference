---
title: CFL Condition
categories:
- Houdini FLIP
---
You may have noticed that there are fields to set min and max substeps on the FLIP solver. Have you ever wondered how Houdini decides how many substeps to use? The short answer is that Houdini checks the CFL condition to determine the number of substeps needed in a given frame. Here I'll try to provide a brief explanation of how the CFL condition works. 

## The Basic Explanation
Say you have a point in a simulation with a velocity. Say that this velocity will push your particle 0.5 units in a frame. 

Each simulation should have a parameter for the particle separation as well as the CFL condition. 

TBC

## Further Reading
If you're particularly math savvy, you can read the original paper describing the CFL Condition from 1928, translated into English, here: https://web.stanford.edu/class/cme324/classics/courant-friedrichs-lewy.pdf