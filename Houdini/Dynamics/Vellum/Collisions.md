---
title: Vellum Collisions
categories:
- Houdini
---

### Character hands or feet are hooking on clothing.
- You may need to remove problematic body parts. It's not uncommon to remove hands to avoid collision issues in cloth.
- Set up collision ignore system.
- Substeps