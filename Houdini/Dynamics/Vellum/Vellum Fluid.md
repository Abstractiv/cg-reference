Vellum now supports particle fluids. This allows you to easily integrate fluids into sims involvings grains, fabrics, vellum rigidbodies, etc. A downside to using a solely particle-based fluid as opposed to a particle-volume based technique such as FLIP is that particle fluids have a tendency to collapse and explode. 

In order to combat this problem, which was much more prevalent during the SPH Fluid days, SideFX has added a Repulsion Force that attempts to maintain separation between particles, as the exploding tended to happen as a result of particles moving on top of each other.-   Let's get rid of our fancy twisting smoke. 

### Phase
Vellum lets you specify an integer phase attribute in order to delineate between different non-mixing fluids. This can be helpful for situations like oil in water, or for adding gelatinous chunks to a more runny liquid. 