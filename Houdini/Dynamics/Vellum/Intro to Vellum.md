---
title: Intro to Vellum
draft: no
categories:
- Houdini Dynamics
tags:
- Houdini Dynamics Vellum
---
## Intro to Vellum
Here we just do a general overview of how the solver works.

### What is Vellum
Vellum is an implementation of XPBD (extended position based dynamics). This builds on the work done on the PBD algorithm. Houdini's PBD implementation is also known as POPs. In this way, we can consider Vellum to be a particle system with constraints. 

In the end, all of the unique behaviors that can be simulated by Vellum come from the different constraints available. This makes sense, as constraints are simply the means by which we tell Houdini how different elements interact with each other. A particle by itself is just a particle, it only begins to behave like water or grains or like a fabric depending on how it interacts with its neighbors.

TBC