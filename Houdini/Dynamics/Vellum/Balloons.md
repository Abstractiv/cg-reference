---
title: Balloons
categories:
- Houdini
---

## General Setup
When you create a balloon setup, you usually create a cloth constraint and a pressure constraint. If you use Vellum Configure Balloon, it'll put that down for you. The inflation of the balloon is controlled by a balance of the stiffness of the cloth stretch and the rest length/scale + stiffness of the pressure constraint. Higher rest length/scale + pressure stiffness = more inflation, although if the cloth is too stiff that'll restrict the amount of expansion you can get. 

You can also control and animate the inflation level by using the @pressurescale attribute. The vellum solver looks for this attribute which typically has values between 0 and 1, where 0 corresponds to the pressure constraint having no influence and 1 corresponding to the pressure constraint having full influence. One thing you could do with this information is attribute transfer pressurescale so that you have inflation happen in proximity to an object, or on cue. Note: If you do this, you'll want to have some blend from 0 to 1 else the inflation will be basically instant and will look strange.

## Pressure Issues
### Inconsistent Inflation
- Make sure the balloon resolution is high enough. Not enough geo means not enough pressure constraints get generated, which is obviously a problem.

- You may need to set the mass and thickness to calculate varying or calculate uniform if there is variation in balloon sizes. From there, you'll want to scale your pressurescale attribute by the mass.

- Simplify the constraints property down.
- Decrease stiffness of the cloth constraint
- Increase the stiffness of the pressure constraint

### Balloon not Deflating all the Way
- There is a decent chance that you need to reduce the bend stiffness of the cloth. If it's too stiff it won't want to bend too far out of shape.
