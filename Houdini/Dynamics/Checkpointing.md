---
title: Checkpointing
draft: no
categories:
- Houdini Dynamics
tags:
- Houdini Dynamics
---
Checkpointing lets you pick up a simulation from a checkpointed frame. For example, if a sim crashes on frame 1030, and you have a checkpoint on frame 1025, you would only need to start simming from 1025 and not from the beginning.

![[checkpointing.png]]

You can access the checkpointing options by going to the "Cache" tab of a DOP Network. Near the bottom is a checkbox called "Save Checkpoints". The Checkpoint interval is how often you'll save a checkpoint. A value of 10 means save a checkpoint every 10 frames. 

The trail length is how long of a record to keep before automatically deleting checkpoint files. A value of 0 means don't delete any checkpoints.

Note that these are .sim files which hold more data than your typical .bgeo file. Bgeo's don't include enough data about a simulation to pick up from, they're mainly used to hold geometry information. As such, sim files are larger and slower to write and read than bgeos. You probably don't want to checkpoint every single frame, generally.