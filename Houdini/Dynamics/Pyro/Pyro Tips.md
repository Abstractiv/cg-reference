---
title: Pyro Tips
categories:
- Houdini
---
Density volumes going into the simulation should be from 0 to 1. If you need a thicker smoke fix it post-sim. *I don't know if I agree with this one. Look into it more.*

The volume sourcing doesn't need to have the same resolution as the simulation.

Volume confinement and shredding are not super compatible. They both have a tendency to shred up the velocity field and using them together can cause ugly chaos. Be gentle if using these in conjunction.

One of the most important factors in getting pyro to move the way you want on the large scale is having a good source. A jet of fire, say from a dragon or flamethrower probably ought to be emitted from a stream of particles from a prior POP sim, for example.

