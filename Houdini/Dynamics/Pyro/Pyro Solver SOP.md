---
title: Pyro Solver SOP
categories:
- Houdini
tags:
- Houdini Dynamics Pyro
---

## Shaping
### Buoyancy
Buoyancy adds a buoyancy force to a velocity field. 

It asks you to specify an ambient temperature (this would be the temperature of the air in the surrounding environment, aka. the temperature of your sim where the temperature field equals 0) as well as a reference temperature (this would be the temperature of your sim where the temperature field equals 1). 

If the temperature of your smoke is the same as the ambient environment, i.e. temperature is zero, then it will not rise. The higher the temperature is compared to the environment, the faster it will rise.

It also provides a gravity parameter. The only use of the gravity parameter is to set the direction of buoyancy, which is always opposite the direction of gravity.

The formula for the final buoyancy lift speed is:
$$ -b * (T_{r} - T_{a}) / T_{a} * g $$
Where 
- b = Buoyancy Scale
- Tr = Reference Temperature
- Ta = Ambient Temperature
- g = Gravity Acceleration

If you want to know more about the Gas Buoyancy DOP microsolver, see the [[Microsolvers#Gas Buoyancy|Microsolvers]] page.

### Wind


### Disturbance


### Turbulence


### Shredding


### Flame Expansion


### Viscosity

