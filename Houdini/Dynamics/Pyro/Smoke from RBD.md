---
title: Smoke from RBD
categories:
- Houdini
tags:
- Houdini Dynamics Pyro
---

## Overview

Often times when things break, there is a bunch of dust that floats in the air and trails behind the broken pieces. This can be easily implemented using the Debris Source SOP. Debris Source scatters points on the inside of fractures when things break apart, and we can use these points as a smoke source for the Pyro/Smoke solver.

![[smokerbd_demo.png]]

Depending on the look you're going for, you may want to [[Houdini/General/Miscellaneous#Compute Velocity|compute velocity]] on the generated points. This will allow you to have smoke that either moves in the direction of or the direction opposite the motion of the broken piece.

You can also trail a bit if your object is fast moving and needs excessive substeps to prevent the source from stepping.