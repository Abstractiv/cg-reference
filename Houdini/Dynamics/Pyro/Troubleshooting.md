### Missing Pyro from Render or during VDB Conversion
By default, pyro will only render or generate VDB based on the bounds of the density field. If you are working with smokeless fire, or you don't have much smoke, this can result in parts of your sim disappearing.

On the Pyro Post-Process (or the post-process section of the Pyro SOP Solver), you need to check on flame density when convertin to VDB or rendering. This will write within the bounds of the flame field (by default) a density value of 0.001.

![[pyropostprocess.png]]