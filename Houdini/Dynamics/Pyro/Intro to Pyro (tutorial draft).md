---
title: Intro to Pyro Tutorial
categories:
- Houdini
tags:
- Houdini Dynamics Pyro
---

Building a basic pyro simulation in Houdini is easier than is suggested. There are three major steps to creating a good-looking pyro simulation:
- Creating a compelling source
- Simulating and tweaking
- Post-sim modification and shading

## Sourcing
### Creating our starting points
Most inputs that you provide to a pyro simulation will be volumes that emit various properties into the simulation. These include density, temperature, burn, and velocity. The recommended workflow for creating these volumes is by creating points with attributes matching the volumes you want, then converting those points to a volume. This can be done with a Volume Rasterize Attributes.

So, let's say we want to set the built-in pig head on fire. We put down the pig head node, then we scatter some points where we want to set our fire. The exact nodes I put down to do this were the Pig Head -> Match Size -> Attrib Paint -> Scatter. The pig head creates the pig head. 

I used match size to get the pig head to rest on the XZ plane. I did this entirely for my own satisfaction, and I did it by setting the Justify Y to "Min". This moves the pig head upwards so that the lowest point on the geometry rests at Y = 0.

The scatter node lets you use an attribute to determine where you want points to be scattered and how densely. By default, it looks for an attribute called "density". In order to paint an attribute called density, on the Attribute Paint node, I went to the "Attributes" tab and set the "Attribute Name" to "density". I then painted where I wanted the points to be scattered. Red is a value of 1, and blue is a value of 0.

I then put down a scatter node and checked on "Density Attribute". This lets the scatter node know that there is an attribute available to read to determine the scattering density.

![[pyrointro_01.png]]

### Giving those points pyro attributes
We now want to create attributes that will be used to create our volume sources. If we want to set the pig's head on fire, we will at the very least require temperature and burn, however we may want to add v to give our fire some initial velocity, or density to give our simulation additional smoke. In this example, I will simply source temperature and burn. Here is a brief explanation of what each attribute/field is used for.

- **density:** Used to source non-emissive smoke, i.e. not fire.
- **temperature:** Used to add a rising force to the sim, aka buoyancy. Without temperature, fire and smoke would not rise upwards. Temperature is also often used as a control field for various pyro features as well as a color source for fire.
- **burn**: The values in burn get written to the flame field inside of the simulation. Essentially, areas with burn will ignite in the simulation, and the value of burn will correspond to the lifetime of the flame as it rises.
- **v**: This gets written to the vel field, and it applies velocity to fields in its vicinity.
Bonus:
- **divergence**: The divergence field adds an outwardly expanding force to your fields. Fields in areas of high divergence will want to diffuse towards areas of low divergence
`Make Diagram`

There are many ways we could create these attributes. Attribute Create, attribute wrangle/VOPs, but in this case we'll be using the Pyro Source node. Pyro Source provides dropdowns for the various attributes you can add to your sim. We'll add two attributes, and select temperature and burn from the dropdown. We can leave the default values at 1 for now. If we check our Geometry Spreadsheet, we should see these attributes added to our points.

We'll want to a bit of noise to the attributes, but before we do so, let's rasterize them (convert them to volumes) then hook them up to the pyro solver so we can see what we're doing.

### Volume Rasterize
Let's convert our point attributes to volumes now.

Put down a Volume Rasterize Attributes and in the "Attributes" field, put the names of the attributes you want to convert to volumes, in this case burn and temperature. You should immediately see a blocky, puffy volume appear. If we check the info panel on that node, we should see two VDBs added to our geo, one called burn and one called temperature.

Each voxel of temperature, for example, should hold the value of the temperature attribute held on the point the voxel was generated from. These voxels are generated in spheres around each point, the radius of said spheres being controlled by the "Particle Scale" parameter. Increase the particle scale will make the volume larger and puffier, and making it smaller will make it the opposite. You want the particle scale to be at least large enough to fill in the gaps between points.

You'll also want to often check on "Normalize by Clamped Coverage". This can help smooth the volume out so that it is more continuous and less blobby, but you'll likely need to increase the particle scale a bit to compensate for the loss of volume. 

Now that we have our sourcing volumes, we can put down a Pyro Solver SOP and hit play. We should now have a rising fire, albeit one that's stretched out and pretty featureless.

TBC