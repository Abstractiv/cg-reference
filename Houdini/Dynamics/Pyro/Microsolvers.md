---
title: Microsolvers
categories:
- Houdini Dynamics
tags:
- Houdini Dynamics Pyro
---
There are way more microsolvers than what I'm listing here, but these are the ones I use most commonly.

## Shaping
### Gas Disturb
Disturb adds a random length adjustment to the existing velocities. If you have set your mode to "Continuous", then it will randomize per voxel. If you set it to "Block-Based", then it will create a random value per block, so two nearby voxels that are within the same block will receive the same random length adjustment. The blocks are subdivided based on the number of octaves, with the base block size being the width of the largest subdivision level.

**Rotational Force**
When checked on, instead of adjusting the magnitude of the applied forces, this instead just rotates the applied force vectors without changing the length.

**ADD DIAGRAM**

If you enable "Rotational Force" near the top, then instead of simply adding the random vector to your velocities, which could lengthen or shorten the velocity vector depending on whether the random vector pointed with or against the original velocity, Houdini will simply rotate the vectors without changing their length. It does this by normalizing the result of the noise added with the original velocity, then by multiplying the new normalized direction with the original length.

The idea behind disturbance is that since it's randomized over small distances with relatively small strength, over a larger simulation the randomizing effect gets averaged out, resulting in a sim that has the same overall shape but with more fine detail.

### Gas Turbulence
Turbulence adds a [[Curl Noise|curl noise]] to the velocity field, usually used to add medium and large scale turbulent flow to the pyro simulation. This will change the overall shape of your sim.

### Gas Shred
Gas Shred uses the gradient of a field to add noise to the velocity of the sim. By default, it takes the gradient of the temperature and pushes and pulls along the direction of said gradient. This pulls apart the sim, adding waviness and streaking. Useful for fire simulation.

### Gas Buoyancy
Gas Buoyancy adds a buoyancy force to a velocity field.

It asks you to specify a buoyancy direction and a buoyancy lift, which can be thought of as a multiplier on the strength of the buoyancy force.

Unlike the interface under the Buoyancy section of the Shaping tab on the Pyro Solver SOP, the buoyancy microsolver does not ask you for a reference temperature. It works only in the range of values provided by the temperature field, i.e. without any standard temperature units such a kelvin. **As such, the ambient temperature parameter is best left at zero as that corresponds to zero on the temperature field.** Unless you know what you're doing, of course. Setting the ambient temperature above 0 will lead to the effect where warmer parts of your sim will rise and cooler parts will sink. Could be useful for convective motion, for example.

The final force is calculated by the equation 
$$ lift * (temp - ambient) * direction $$

### Gas Damp
Gas Damp behaves similarly to drag in other simulation types. Every frame, the velocity will be scaled by 1 - Scale. In other words, a scale of 0.1 will remove 10% of the velocity in a frame, as the tooltip will note. There are a few other handy parameters besides scale, however.

Target speed is the minimum speed at which a velocity will be scaled. If the velocity is lower than the target speed, then no damping will be performed.

### Gas Vortex Confinement
Vortex confinement detects areas of high vorticity and boosts velocities in those areas in order to preserve vortex motion. This can have the effect of introducing more chaotic motion, as vortices that may have gotten smoothed out without confinement will have a much larger impact on the sim. Best used in moderation.

### Gas Vortex Boost
Vortex Boost adds confinement to vortices of a specific size/energy. You provide a swirl size, which is the width of the vortices you want to amplify.

### Gas Vortex Equalizer
This microsolver lets you perform and manage multiple vortex boosts at once.

## Utility
### Gas Cross
The Gas Cross microsolver performs the crossproduct between two vector fields. Sources A and B will be the two fields your performing the cross product on, and the Dest Field is where you want to write the result.

### Gas Blur
There isn't much to say about this one besides that it blurs the provided field. This can be used, for example, to add rudimentary viscosity to a smoke sim or to diffuse a temperature field. Can optionally be time-dependent.

### Gas Match Field
This microsolver is used to create a new temporary field that matches in size and resolution to the provided reference field. 

This is useful if, say, you want to make a data field that has the same size and resolution as density, perhaps to use as a control field or to as some kind of calculation field, but that you don't want to have to source beginning or export at the end of the simulation.

### Gas Limit
Applies a limit to values in a field, i.e. it clamps the values. Similar to the POP Speed Limit, but can be applied to more than just speed.

## Additional Resources
https://www.artstation.com/artwork/gJ3glL