---
title: POP Replicate
categories:
- Houdini
---

### Death Point Mismatch
If you need your pop replicate burst to trail directly from the parent particle's death point, you need to disable jitter and disable 'reap at end of frame' on the POP Solver.