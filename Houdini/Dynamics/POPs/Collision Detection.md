---
title: Collision Detection
categories:
- Houdini
---

### Inconsistent Detection
Trying to detect collisions against specific objects and having inconsistent results with POP Collisions Detect? Just use a wrangle and reference the @hitprim attribute. Make sure you're adding hit attributes (check the POP Solver), of course.
