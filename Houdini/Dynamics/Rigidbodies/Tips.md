---
title: Tips
draft: yes
categories:
- Houdini
---
**Wedge Testing**

**Animated object not working**
If you're doing the transform pieces trick, make sure your pieces are packed before they go through the Transform Pieces node.