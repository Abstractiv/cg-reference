---
title: RBD Fractured Object
categories:
- Houdini
---
Did you give your RBD Fractured Object a velocity but nothing is registering? Make sure you check the 'Inherit Point Velocity' button on the RBD Fractured Object DOP.