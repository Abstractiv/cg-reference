---
title: Optimizing
categories:
- Houdini
---
**Fracturing**
Only fracture parts that will actually be destroyed if possible. If it's not convenient or feasible to do this, make sure the parts that won't be destroyed are clustered and inactive.

**Use Animated Object**
Using an inactive, animated object instead of a deforming collision object will help with performance. If you have an object that is for all intents and purposes animated but is technically deforming you can drop down an Extract Transforms node and run it through a Transform Pieces node. 

You can also convert the rest pose to spheres and use a point deform to make your sphere'd up model follow the animation. Extract transforms, etc. as needed.

**Use Glue when possible**
Houdini has luckily made it very easy to convert a glue constraint to a soft constraint upon breaking. Glue is much cheaper to calculate, and so it is advantageous to use glue until it is necessary to switch to soft. 