---
title: Collision Ignore
categories:
- Houdini
---
Collision ignoring is set with the @collisionignore and @collisiongroup attributes.