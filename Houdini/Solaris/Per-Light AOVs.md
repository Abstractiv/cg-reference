---
title: Per-Light AOVs
categories:
- Houdini Solaris Redshift
---
## Karma
In Karma, you'll need to determine the light groups using LPE Tags. If you go to the Karma tab on the Lights, you'll see near the bottom is an option to specify an LPE Tag. Go ahead and give your lights whatever tags you want.
![[karma_lightlpetag.png]]

Then on the Karma Render Settings node under Image Output > AOVs (Render Vars), on each shading AOV you're exporting, you'll check on "Split per LPE Tag". That will automatically create different AOVs for each light for each AOV that is turned on for.
![[karma_splitperlpe.png]]

## Redshift
In the OUT context, setting up per-light AOVs for all of your lights was a pretty straightforward process. Hit a dropdown item and it automatically created a bunch of AOVs for you for each light. Unfortunately, as far as I know, you have to manually make each AOV for each light group in Redshift's Solaris implementation.

So, say you have three light groups, one for red lights, one for green lights, and one for blue lights. They are called red, green and blue respectively. 

**Note:** If you don't know how to put something into a light group in Redshift, all you do is go to the Light, and under the Redshift tab, expand the Light Group box and type the name of the group you want to put the light in. You don't create a new group anywhere beforehand, if you want to put a Light into a group called "red" and that group doesn't exist yet, just type red in there anyway and it will create it for you.
![[rssolaris_perlightlight.png]]

Once you've put the lights you want into their light groups, you manually make all the AOVs you need. If you have three light groups you want to create AOVs from, you'll need to create three AOVs for each render component. For example, three reflection AOVs, diffiuse AOVs, specular AOVs, etc. In this example here, I've created three of each, and given each a unique name.

Then, to link each AOV to the desired light group, just open the "Light Groups" dropdown and type in the name of the light group you want to tie the AOV to.

![[rssolaris_perlightaov.png]]