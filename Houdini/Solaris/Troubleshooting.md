---
title: Troubleshooting
categories:
- Houdini Solaris
---
### Volumes not Appearing
Make sure you have a density field and it is populated with data. See [[Houdini/Dynamics/Pyro/Troubleshooting#Missing Pyro from Render or during VDB Conversion|this page]] for more info.

If you are working with multiple volume sets, you may need to set the usdvolumepath attribute.

### No Motion Blur
Remember to explicitly set the Primitives parameter with either the primitive/mesh name or a wildcard.

Make sure you have velocities under the Scene Graph Details for the problematic primitive.

Make sure velocity blur is ticked on for the correct renderer. 

Make sure you didn't disable it in the render settings.