---
title: Kinds
draft: yes
categories:
- Houdini Solaris
---
When organizing your Scene Graph Tree, one of the organizational methods used is known as "kinds". There are three different "kinds" that can be used for organization.
- Assemblies
- Groups
- Components

Assemblies and Groups are essentially the same thing, however making something an assembly versus a group marks it as a more important object. Groups and assemblies can have other kinds of objects underneath them in the hierarchy. Components can't, however. Usually, when you bring in geo, you want that geo to be within a component. Then, if that component is part of something larger, then you put your various components under a group.