## Where is the Global Variables node
This one stumped me for a bit. I needed access to info as you would find on the Global Variables node or the RS State node in Redshift. In Karma's MaterialX implementation, you can find that kind of information on the Karma Ray Import node. Hit the dropdown on Variable Name to see your options.

![[mtlx_rayimport.png]]

## Fresnel
See this bit of documentation to see how to set up a fresnel shader in MaterialX: https://www.sidefx.com/docs/houdini/nodes/vop/hmtlxfacingratio.html#how_to

Note: you'll need to expand the How-To section.