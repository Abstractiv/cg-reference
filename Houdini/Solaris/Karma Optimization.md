---
title: Karma Optimization
categories:
- Houdini Solaris
---

Path Traced Samples: 64

**Rendering**
	**Camera Effects**
	* Disable Depth of Field

**Image Output**
	**AOVs (Render Vars)**
		* Pixel Filter: mitchell
		* Pixel Filter Size: 1

**Advanced**
	**Sampling**
		* Convergence Mode: Path Traced
		**Primary Samples**
		* Minimum Samples: 3 (unclear if this was changed)
		* Variance Threshold: 0.05
		**Buckets and Caching**
		* Image Mode: Bucket
		* Progressive Passes: 1
		* Bucket Size: 8
