---
title: SOP Import
categories:
- Houdini
tags: 
- Houdini Solaris Redshift
---
## Basics
To use the SOP Import node, the bare minimum you need to do is put the node down and reference a SOP path. From there, you can wire your SOP import into the chain, perhaps into a merge with other geo, or into a material library/assign material. 

If you look at your Scene Graph Tree, you'll likely see that your Geometry you imported has the name sopimport1, or something along those lines. That name, and where the geo is stored in the tree, is configured in the Import Path Prefix parameter. By default, it's /$OS, which means the geo will be stored at the root level, and it will have the name of its SOP Import node. Thus, if we want to give the object a more descriptive name, we'll either need to rename the SOP Import node, or manually set a new name in this field.

### Importing your Groups
By default, geometry groups don't get imported into Solaris. There is a similar concept, however, called Geometry Subsets whose primary purpose is to be used for material assignments. Under Import Data -> Subset Groups, you can enable the use of these subsets. You just need to enter the names of the groups you want to bring in as subsets, and if you view your scene graph tree from here, you should be able to see your Geometry Subsets as children of your primitive.

## Volumes
If you want to import multiple pyro sims and have them be put under unique volume primitives, you need to give each sim a usdvolumepath primitive string attribute before you import.