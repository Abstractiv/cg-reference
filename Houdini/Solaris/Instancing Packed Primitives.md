---
title: Instancing Packed Primitives
categories:
- Houdini Solaris
---
### Slow Instancing
Trying to load huge numbers of packed prims into Karma is painful by default because it loads the instances as Native Instances by default. To change this to use a point instancer, which can handle much more geometry, more in line with what you're probably used to with instancing, you can either use a USD Configure SOP or configure the same settings on the SOP Import or whatever you're using. Under Primitive Definitions, there is a setting to Treat Packed Primitives As where you can hit the dropdown and switch it to Use Point Instancer. 

The point instancer is a bit more limited than using native instances, but if you're trying to handle disgusting amounts of instances then you're better off using it anyway.

### Redshift Instance File (OLD METHOD)

This is the way to do this prior to Redshift 3.5.09. For an more up to date guide on this, see [[Redshift Instance Files]].

![[rs_instance_file.png]]

Instancing from a proxy file in Redshift in Solaris is poorly documented at this time. Some things I have learned from experimentation and scouring the forums:

To set up the proxy file, you need to create some stand-in geo. In this case, I just used a cube primitive. Note: Using a SOP Create as the stand-in does not work in Houdini 19. **The name of the primitive must end in "_rsproxy"**. 

To replace the stand-in geo with the proxy file, you need to use a Render Geometry Settings node. Go to the Redshift -> Proxy, and under the proxy file parameter, reference the rs file you previously saved out. 

After that, you can put the proxy geo into an instancer like anything else.