---
title: AOVs
categories:
- Houdini
- Solaris
tags:
- Houdini
---
Inside of USD, AOVs are known as Render Vars. There is a dedicated Render Var node for settings these up. If you're working inside of Karma, there is a digital asset that bundles a bunch of standard ones up that you can use instead of building them manually. In Redshift you have to build these manually, unless you make your own asset.

---
## Setting up (Post-Redshift 3.0.67)
There is now a redshift rendervar subnet that provides the same functionality as the AOV tab on the Redshift ROP. Place that down prior to your output LOP and you should be able to easily set up AOVs the old-fashioned way.

## Setting up (Prior to Redshift 3.0.67)
As previously mentioned, you can use the Render Var node to set up AOVs. Each render var node can be used to set up a single AOV, and it saves to the USD scene tree as a Render Var primitive. 

The name of the pass that we need to fetch with redshift is prefixed "rs:". For example, the redshift pass for reflection is called "rs:reflections". You'll want to make sure the output data type is Color3h (by default outside of Solaris RS AOVs mostly use half-float (16-bit) color accuracy). One notable exception is Z-depth, which uses 32-bit color depth by default (Color3f).

![[AOVs_RenderVarParams.png]]

Once you have your AOVs assembled, you'll want to reference them inside of a Render Product. Put down a render product node, and in the 'Ordered Render Vars' menu, you can either list your AOVs in the order you want them to appear in the MPlay dropdown, or use a wildcard to have them be listed alphabetically. 

![[AOVs_RenderProduct.png]]

In this case, I said I wanted my beauty pass to be listed first, then every render var after that to be listed alphabetically.

## Custom AOVs in Redshift

![[solaris_custom_aov_rs.png]]

To use custom AOVs in Redshift in Solaris,  you need to use rs:custom as the source name, regardless of the name of your custom AOV. The name you put into the name slot needs to match the AOV you created in the material.