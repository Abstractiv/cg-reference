---
title: Karma XPU Custom AOVs
categories:
- Houdini Solaris
---
### Step One: Set up the Attribute

Let’s say, for example, we want to make a custom puzzle matte type AOV in Karma XPU. We’ll start by making an attribute named puzzle, and give a different value for the different elements we want to separate. Our rubber toy will get a value of <1,0,0>, our pig head will get a value of <0,1,0>, and our Tommy will get a value of <0,0,1>.

![[xpuaov_attribsetup.png]]
![[xpuaov_attribvis.png]]

If we visualize puzzle as a color…

### Step Two: Set up the Material

In /stage, we’ll need to set up the AOV both in the Material as well as in the Karma Render Settings. Let’s start by setting up the Material. Inside of our Karma MaterialX Subnet, we need to bind in our puzzle attribute using a Mtlx Geometry Property Value node. Make sure to set the signature to Vector 3 or Color, and set the Geomprop to the name of the attribute, in our case “puzzle”.

Next , we need to hook our attribute into a Karma AOV node to register that we will be using this attribute in an AOV later. Make sure to set the Type correctly, and make the Name the name of the attribute, “puzzle”.

Next, our AOV needs to be added to our shader. We can’t just leave our AOV free floating, the output needs to make its way into the shader at some point. That said, we don’t want to use our attribute value to affect any of our shading properties like our Base Color or anything else. So what we’ll do is multiply our AOV by 0 and add it to our Base Color. That way, the AOV signal makes its way into the shader without actually changing the Base Color value.

![[xpuaov_material.png]]

### Step Three: Set up the Karma Render Properties

In the Image Output tab, we’ll need to go to the bottom and add an Extra Render Var. In USD-speak, a render var is just another word for AOV. We’ll name our AOV “puzzle”, and say we want to export it as a color3f. Lastly, we’ll set the “Source Name” to “puzzle” as well, as this is the name of the AOV we set up in the material on the Karma AOV node.

![[xpuaov_rendersettings.png]]

### Step Four: Check the Render

Do a test render, and switch to your newly created puzzle AOV. If everything was done correctly, you should have an image like so:

![[xpuaov_render.png]]

If it comes in black, something was incorrectly configured.