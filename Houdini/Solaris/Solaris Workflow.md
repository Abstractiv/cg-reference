---
title: Solaris Workflow
categories:
- Houdini
tags: 
- Houdini Solaris Redshift
---
Working in Solaris is a paradigm shift from the typical Houdini rendering workflow, however a lot of the same stages are present, just organized differently into a single context. You still create your geometry, create and assign materials, set up cameras, lights, and various render settings. None of that has changed. What has changed is the terminology and the logic of the interface. 

Note: If you're not quite ready to make the leap into Solaris, or your looking for something really quick to render old Mantra scenes in Karma, take a look at [[The Fast Workflow (Karma ROP)]] for a very quick intro to the Karma ROP.

## Geometry
There are a few different ways to process geometry into LOPs. These include:
- SOP Create
	- This allows you to create SOP geometry directly from within stage. This would be akin to putting down a Geometry node in the OBJ context.
- SOP Import
	- This will bring in a SOP geometry from the OBJ context. Note that this does not include materials, and if you want to bring in groups, you'll need to configure Geometry Subsets in the SOP Import parameters. See [[Importing Geometry]] for more info.
- Component Builder
	- This is the SOP Create on steroids. In addition to being able to create geometry, you can also create and add materials to this asset, as well as prep the asset for use in Solaris's layout tools and Asset Gallery. See [[Component Builder]] for more info.

Regardless of how your geometry is brought into or created in Solaris, you can still set your top-level geometry settings in much the same way. This is done using the Render Geometry Settings node, and it carries the interface you would normally find at the OBJ level of a Geometry node.

### Component Builder

---
## Materials
Setting up material subnets is roughly the same as before, however the way you assign them has changed. Instead of setting materials on a Material node at the SOP level or under Render > Material at the OBJ level, you assign them using the Assign Material node in LOPs. From there you can drag the primitive or geo subset you want to assign a material to to the Primitive slot, and you can drag the corresponding material to the Material slot.

**Note about Redshift**
One thing to be wary of when trying to copy your Redshift materials from the old workflow to Solaris is that RS Material Builders use a different output node in Solaris. If you just copied your material builder from /mat or a matnet, you'll need to drop inside and replace the Redshift Material node with a Redshift USD Material node. Besides this, everything else should work out of the box.

---
## Cameras
Cameras work much the same way in Solaris as in /obj.

Parenting can be achieved using the Parent Constraint LOP.

---
## Lights
Lighting workflow has been greatly expanded in Solaris compared to what most people are used to in /obj. 

### Light Mixer
The light mixer allows you to have a single interface to control light settings, ranging from color, to intensity to exposure. It also adds viewport controls to add additional lights that create specific specular or diffuse highlights.

See [[Light Mixer]] for more info.
[Light Mixer](cg-reference/Houdini/Solaris/Light Mixer.md)

---
## Rendering
### AOVs
AOVs are now know as Render Vars inside of USD/Solaris. These are set up one at a time using Render Var nodes and are compiled into a "Render Product". See [[AOVs]] for more info.

### Issues
Rendering works in the viewport but I can't render to disk or Mplay: Your $HOUDINI_PATH environment variable may be set incorrectly. You need to make sure the `;&` is present at the end, even if you're using system variables instead of the houdini.env. This allows Houdini to add additional variables at runtime.

