---
title: Light Mixer
draft: yes
categories:
- Houdini Solaris
---
The light mixer adds advanced light-adjusting and light-placing tools.