---
title: Redshift Instance Files
draft: no
categories:
- Houdini Solaris
tags:
- Houdini Solaris Redshift
---
As of Redshift 3.5.09, there is a dedicated LOP for importing .rs proxy files. These can be used as the prototypes for an instancer.

![[rs_instanceproxy_solarisLOP.png]]

If you are just importing one proxy, great!

If you need 50 unique proxy files for differing pieces of debris, this doesn't help us much, as the Redshift Proxy node can only reference a single proxy file.

I'll be honest, I feel there has to be a more elegant solution to this problem, however the following is *a* solution. We can create multiple primitives from multiple files using a for-each loop. As you can see though, the Redshift Proxy node doesn't have an input, so it can't be put into a for-each loop. What you can do is double-click inside and copy the contents out and put them into the for-each loop. If we have 50 proxies, you'll want to set the "Iterations" number on the foreach_end to 50.

![[rs_instances_foreach.png]]

Going into the loop, the first node from inside the proxy node is a Primitive node that creates the empty RedshiftProxy USD Primitive. The big mostly empty text box is used to set the name for the primitive. By default it's set to "/$OS", meaning it'll place the new primitive in / under the name of the node. We'll want to set a custom name here, as well as a unique number for each iteration. If we want create fifty primitives called debris\_#, we'll use the syntax /debris_\`@ITERATION\`. This means that on iteration 5 of the loop, you'll get a new prim called "debris_5", on iteration 10, "debris_10", and so on.

Next, we'll need to edit the "Edit Properties" node to point to the correct file location. First, delete all the channels. Ctrl+Shift+LMB each blue parameter. In the fifth text field, put in the path to your proxy, and substitute the file number with \`@ITERATION\`. Now, each debris proxy primitive will reference a different file on disk.

![[rs_instance_editproperties.png]]

Finally, hook the foreach_end into the second input of an Instancer. On the first input, connect some points, in the case I chose and external SOP with points that have a "variant" integer point attribute, which holds a random number from 0 to 49. 

In the Prototypes section, reference all the debris objects you just created using the syntax "/\*". You can match up an instance to its corresponding variant number on the points by opening the "Options" submenu, changing "Prototype Index" to "Index Attribute", and setting "Index Attribute" to the name of the integer point attribute you're using to select your piece variants.

![[rs_instances_instancer.png]]