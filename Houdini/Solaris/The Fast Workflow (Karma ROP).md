---
title: The Fast Workflow (Karma ROP)
categories:
- Houdini
tags:
- Solaris Houdini
---
## Why?
Okay, say you want to test out the abilities of the new Karma renderer but you're not quite ready to learn an entire new context. The Karma ROP makes using Karma a breeze if you're used to the old workflow. Essentially, everything should work out of the box in the same way that Mantra did. Under the hood (inside the HDA) there is a LOP network that holds a bunch of scene imports taking care of geo + materials, as well as lights and cameras. 

Of course, you won't be able to take advantage of all the new layout and lighting tools available in Solaris this way. I'm also not yet sure how to adjust render settings on specific geometries without cracking open the HDA. These are the tradeoffs. Still, this can be a convenient way to prototype, to re-render old Mantra scenes, or to do smaller, simpler scenes while still leveraging Karma.