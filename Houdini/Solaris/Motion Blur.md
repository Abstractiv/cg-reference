---
title: Motion Blur
categories:
- Houdini
tags:
- Solaris Houdini Redshift
---
Outside of Solaris, you may be used to enabling velocity blur in the Redshift tab on your Object parameters. In Solaris, however, you may notice that those settings are not present on any of the SOP Import/Create/etc nodes. That is because those settings now live on their own settings node. You'll find this is a general trend within Solaris, as Render settings also live on their own node in the same way.

Motion Blur settings are now largely split between the Render Geometry Settings node and the Camera node.

In order to turn on velocity blur, put down a Render Geometry Settings node (I usually do this right after the import). You must specify a primitive or set of primitives, the default lopinputprims('.', 0) expression will not work. Redshift allows you to use the top level primitive name, however Karma requires you to specify a Mesh primitive.

If you want to mass enable velocity blur for everything, just this the little dropdown and press "All Mesh Primitives".

**Update 19.5**
The is now a motion blur LOP to adjust some general motion blur settings. If you're using velocity blur you'll still want to turn that one per object on the Render Geometry Settings node.