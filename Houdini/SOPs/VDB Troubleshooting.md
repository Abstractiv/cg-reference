---
title: VDB Troubleshooting
categories:
- Houdini
---

“VDB primitive density was skipped because it is not a level-set grid.”

This tends to happen when you convert a non-SDF Houdini Volume to a VDB SDF and then try to run the VDB Reshape SDF. In order to fix this error, you need to first convert your volume into a Houdini SDF. Do this by plugging the volume into an IsoOffset node and setting the mode to Volume Sample. Remember to set the name and voxel size. Now you can convert to a VDB and operations should run smoothly.