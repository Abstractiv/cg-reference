---
title: Tips and Tricks
categories:
- Houdini
- Hair
---

### Sculpt with Physics
The sculpt with physics mode in the guide groom has a bad habit of deleting the skinprim and skinprimuv attributes. If this happens, you may need to unlock the asset, go the attribute delete node near the top, and manually exclude those two attributes from deletion. Otherwise you may run into trouble when trying to do final hair generation.

### Mirroring Guides
Sometimes you can get away with using a regular old mirror node, but sometimes you want something a little more custom geared for grooming. The Guide Groom node has a mirroring section that works similarly to the mirror node. 

### How to Make Hair Fall Out at the Root
If you want hair to fall out at the root, you want to set the glue to animation attribute on the root points (there should be an existing roots point group) to zero. You also want to set the mass to 1.0 or something. Something that matches the rest of the hair.

### Setting different colors at root and tip in Redshift
In Mantra, you have the option to color the tip and root differently in the shader, but at first glance the Redshift shaders don't offer this functionality. The key here is to using the RS Hair Position node. 

See [[Hair Rendering]] for a more detailed breakdown of hair rendering in Redshift.

### Custom Clumps
You can input hand drawn curves into the fourth input of the clump node in order to define custom clumps.

### Making guide partition work better
Make sure to check the resolution of the resample. This is my problem most of the time, the default Segment Length of 0.05 is way too large for a regular sized head.