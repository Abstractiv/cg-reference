---
title: Shorter Hair Workflow
categories:
- Houdini Hair
tags: 
- Houdini Hair
---
## Research
Hair dressing tutorial: [Youtube](https://www.youtube.com/watch?v=Qni0rISpqnk "https://www.youtube.com/watch?v=Qni0rISpqnk")

I would start the lesson by watching this entire video with the students. Talk about some of your thoughts of how to approach this problem based on the contents of the video. 
For example:
- Talk about separating the groom into regions.
- Talk about setting the initial length of each region, then moving it into rough shape and doing initial cutting.

Also talk about workflow about setting up things like eyebrows, facial hair, and **peach fuzz**.

---
## Setting up the Skin Geometry
Bring in the head geometry, and cut away anything besides the head. Anything more than the head is unnecessary for such a short haircut. If you're working with long hair, then you may want to keep more of the body as a collision object.

After isolating the head, use a polyfill to close the shape, and make sure the normals are correct.

**Skin**
As an example, you can subdivide and paint a density attribute, however for the final workflow we will not use density to create guides. 

**VDB**
Convert your closed head shape into a VDB that you can use as a collision object on the groom object.

---
## Hair Groom
We're going to break the head down into five different regions according to the regions that the hairdresser splits the head into in the youtube video attached at the beginning. The workflow in each region will be about the same, and will be as follows.

Sketch Regions -> Plant Guides -> Set Initial Length -> Trim to correct length -> Pre-mirror Shaping -> Post-mirror Shaping -> Merge Regions + Apply Attributes -> Procedural Guide Processing -> Create Parting Lines

---
## Hair Generation

---
## Rendering