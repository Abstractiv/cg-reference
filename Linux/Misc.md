---
title: Misc
---
### Clear Login Keyring
Login Keyring prompt keeps popping up and won't let you clear it? Try renaming your login.keyring file so that it generates a fresh one for you.

`sudo mv ~/.local/share/keyrings/login.keyring ~/.local/share/keyrings/login.keyring.old`

The next time the prompt pops up, enter your login password and it shouldn't bother you from there.

### Grep Content Inside Files
If you want to return a list of files in a folder with a certain keyword...
`grep -RH <keyword> <directory>`
`-R` : Search recursively
`-H` : Return filenames

### Using your user monitor settings for you login screen
Because sometimes you want your login prompt to not be on the TV that is barely out of sight.
`sudo cp .config/monitors.xml /var/lib/gdm3/.config/`
This fixes the owner and permissions on the copied file:
`sudo chown gdm:gdm /var/lib/gdm3/.config/monitors.xml  && sudo chmod 755 /var/lib/gdm3/.config/monitors.xml`

### Convert videos with ffmpeg
Without re-encoding:
`ffmpeg -i example.mov -c copy example.mp4`

### Extracting with tar
> `tar -xvf foo.tar`

> `tar -xvzf foo.tar.gz`

> `tar -xvjf foo.tar.bz2`

> `tar -xvzf /path/to/yourfile.tgz`

### Access root crontab
> sudo crontab -e

### Reset tracker-miner-fs
> `tracker reset --hard`

You can also just delete the tracker database:
> `rm -r ~/.cache/tracker`


