---
title: Handy Programs
---
This is here for when I inevitably wipe my system without thinking and forget what programs I had and liked.

---
### QDirStat
QDirState returns in-depth disk usage info.

---
### Feeds
My preferred RSS feed reader at this time.
https://gfeeds.gabmus.org/

---
### Spot
GTK-based Spotify client.
https://flathub.org/apps/details/dev.alextren.Spot

---
### ClusterSSH
Useful for running multiple synced SSH sessions. I put a list of machines in my $HOME/.clusterssh/tags file with the tags I want to assign.

Format:
<hostname01> <tag> <...>
<hostname02> <tag> <...>
...

---
