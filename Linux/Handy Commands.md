---
title: Handy Commands
---
**fsck**
Sometimes a system will crash and trying to boot will throw you to a black screen. In that case, it'll probably spit you into initramfs where you can run fsck to verify and repair your disk. To do so, run `fsck -f -c -y <device>`. An example device name would be /dev/sda2.

**find**
Example:
````bash
find / -name <filename>
````
This searches / for a file of the provided name

**nmap**

**ip addr**
Use this one to fetch your local IP address and other important details about your network.

**ssh and scp**
SSH is used to log into a terminal session on a remote computer.
SCP is used to copy files back and forth from a remote computer.
````bash
scp [OPTION] [user@]SRC_HOST:]file1 [user@]DEST_HOST:]file2
````

**du**
Use `du` to check stuff about your file structure. For example, you can use `du` to check file sizes. Use the --exclude flag to remove directories from your check.
Ex. 
````bash
du -ch --exclude='/home' --exclude='/cg'
````


**df**
Use to check how much available space is remaining on a given filesystem.

Alternative: **duf** is a nice looking alternative to df. I have slight qualms about the column layout, but I reckon it wouldn't be the most difficult thing to reconfigure...

## Retrieving Hardware Information
**dmidecode**
Use this to retrieve information about the hardware of the system, such as the motherboard model, CPU, and RAM. Can be restricted using the --type argument. Options include   
	bios
	system
	baseboard
	chassis
	processor
	memory
	cache
	connector
	slot

**lspci**
Returns information about hardward plugged into the PCI ports on the motherboard, eg. graphics cards.

**lscpu**
Returns information about the CPU.

**lshw -short -C disk**
Returns information about storage drives.

## Copying MAC Address to File Named after Hostname
````bash
cat /sys/class/net/$(ip route show default | awk '/default/ {print $5}')/address >> "$HOSTNAME.txt"
````

### Simulate Login Shell
This is handy if, for example, you need to see how a specific environment variable is being set.
```bash
PS4='+$BASH_SOURCE> ' BASH_XTRACEFD=7 bash -xl 7>&2
```
From Stack Overflow:
	That will simulate a login shell and show everything that is done (except in areas where stderr is redirected with `zsh`) along with the name of the file currently being interpreted.
	So all you need to do is look for the name of your environment variable in that output. (you can use the `script` command to help you store the whole shell session output, or for the `bash` approach, use `7> file.log` instead of `7>&2` to store the `xtrace` output to `file.log` instead of on the terminal).
	If your variable is not in there, then probably the shell inherited it on startup, so it was set before like in PAM configuration, in `~/.ssh/environment`, or things read upon your X11 session startup (`~/.xinitrc`, `~/.xsession`) or set upon the service definition that started your login manager or even earlier in some boot script. Then a `find /etc -type f -exec grep -Fw THE_VAR {} +` may help.

### Convert mp4 to gif
```bash
ffmpeg -ss 30 -t 3 -i input.mp4 -vf "fps=24,scale=320:-1:flags=lanczos,split[s0][s1];[s0]palettegen[p];[s1][p]paletteuse" -loop 0 output.gif
```

From: https://superuser.com/questions/556029/how-do-i-convert-a-video-to-gif-using-ffmpeg-with-reasonable-quality

NOTE: -ss and -t are both arguments to change the length of the gif, -ss is used to cut from the beginning of the video. `-ss 30` means cut the first thirty seconds of the input. The argument `-t` is used to set the total length of the gif. `-t 3` means that we will use three seconds of the input footage.

### Better file readout
You would typically spit out the contents of a text file using `cat`, however if you intend to read said contents, `bat` is a nice alternative that adds additionally formatting to the output.

## SELinux Nextcloud Issue
https://serverfault.com/questions/396036/apache-httpd-permissions
You'll need to change the SELinux labels for /usr/share/nextcloud and /usr/sbin/httpd.
You can use `sudo chcon -R --reference=/var/www/html /usr/share/nextcloud` and `sudo chcon -R system_u:object_r:httpd_exec_t:s0 /usr/sbin/httpd`