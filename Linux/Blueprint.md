When looking at XML, when a type is defined, for example:

```xml
<object class="AdwToolbarView">
  <child type="top">
    <object class="AdwHeaderBar"/>
  </child>
...
```

The corresponding Blueprint looks like:

```
$AdwToolbarView {
	[top]
	$AdwHeaderBar {
	...
	} 
...
```