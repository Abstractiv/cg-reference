Stop the mysql service
`sudo systemctl stop mysql`

Enter safe mode
``sudo mysqld_safe --skip-grant-tables

In a new terminal, open mysql as root
`mysql -u root`

Run the following commands in order to reset the root password (subsituting 'password' with the new password)
````sql
UPDATE mysql.user SET authentication_string=PASSWORD('password') WHERE User='root';
````

```sql
FLUSH PRIVILEGES;
```

Shtudown the safe mode mysql server
`mysqladmin -u root -p shutdown`
If it starts complaining about some missing unix_socket plugin, you'll need to re-enter the database and run
```sql
UPDATE mysql.user SET plugin = '' WHERE plugin = 'unix_socket';
```

```sql
FLUSH PRIVILEGES;
```

Start normal mysql server again.
`sudo systemctl start mysql`