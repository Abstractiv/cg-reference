---
title: Vector Calculus Identities
draft: yes
---
**The divergence of the curl of a vector field is always zero:**
$$\nabla \cdot (\nabla \times \vec\psi) = 0$$
**The cross product of two gradients is always divergence free**
$$\nabla \cdot (\nabla\phi \times \nabla\psi) = 0 $$
