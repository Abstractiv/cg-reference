---
title: Optics
draft: yes
---
## Basic Terms
**Scattering**
Scattering is a process by which light has its direction altered.
**Absorption**
The removal of energy from the electromagnetic field. 
**Extinction/Attenuation**
The sum of scattering and absorption in a medium.

A key parameter that dictates how a light particle behaves in a medium is the particle sized, described by the following function

$$\pi D/\lambda$$
Where D is the particle diameter and lambda is the wavelength.

## Resources
https://www.gfdl.noaa.gov/wp-content/uploads/files/user_files/pag/lecture2008/lecture3.pdf