---
title: Matrix Basics
draft: no
---
## Basic Notation
A matrix is a rectangular array of numbers. We tend to label matrices using capital letters, and its elements using lowercase letter, with subscript indicating the row and the column. For example:
$$A = \begin{bmatrix}
a_{11} & a_{12} \\\
a_{21} & a_{22}
\end{bmatrix}$$
And to reference a specific elements the syntax is:
$$ A=(a_{21}) $$

The dimensions/size of a matrix with _n_ rows and _m_ columns would be n X m

A matrix with either a row dimension or a column dimension of 1 is known as either a row vector or a column vector, respectively.

## Transpose of a Matrix
 The transpose of a row matrix `1 x m` is a column matrx `m x 1` that has the same numbers in the same order, but obviously the row matrix is horizontal and the column matrix is vertical. If we take an example matrix B:
 $$B = \begin{bmatrix}
2 & 3 & 4 \\\
8 & 5 & 7
\end{bmatrix}$$
The transpose matrix would be labeled B'. If we consider B as a set of two row vectors, we can form B' as being a set of the transposes of those two vectors. Essentially:
 $$\begin{bmatrix}
2 & 3 & 4
\end{bmatrix}
\begin{bmatrix}
8 & 5 & 7
\end{bmatrix} =>
\begin{bmatrix}
2 \\\ 3 \\\ 4
\end{bmatrix}
\begin{bmatrix}
8 \\\ 5 \\\ 7
\end{bmatrix}$$
 $$B' = \begin{bmatrix}
2 & 8 \\\ 3 & 5 \\\ 4 & 7
\end{bmatrix}$$
Consequently, if the dimensions of B are `2x3`, the dimension of B' will be `3x2`.
Furthermore, 
$$(B')' = B$$
## Matrix Addition
Matrix addition is different from regular addition. Matrix addition can only happen between two matrices of the same size and that have elements from the same system, eg. the real number system.
$$ A + B = (a_{rs} + b_{rs}) $$
Written with matrices, 
$$
A\begin{bmatrix}
2 & 8 \\\ 3 & 5 \\\ 4 & 7
\end{bmatrix} + 
B\begin{bmatrix}
1 & 6 \\\ 2 & 4 \\\ 9 & 8
\end{bmatrix} = \begin{bmatrix}
2+1 & 8+6 \\\ 3+2 & 5+4 \\\ 4+9 & 7+8
\end{bmatrix} = \begin{bmatrix}
3 & 14 \\\ 5 & 9 \\\ 13 & 15
\end{bmatrix}
$$
## Scalar Multiplication

## Matrix Multiplication
This one is slightly different. We don't just multiply the matching elements like we did when we added. We multiply the row vectors of A with the column vectors of B. For each pair of A rows and B columns, we multiply each corresponding element and sum them together. The final result of this summing for each column in A will be the value of the element of the matrix at the end. Consequently, A should have the same number of columns as B has rows.

We can express this mathematically as follows:
$$AB = \sum_{k=1}^{m}a_{rk}b_{ks} $$ where m is the number of columns in A/rows in B. As an example:
$$
\begin{bmatrix}
2 & 8 \\\ 3 & 5 \\\ 4 & 7
\end{bmatrix}
\begin{bmatrix}
1 & 6 & 2 \\\ 4 & 9 & 8
\end{bmatrix} = \begin{bmatrix}
2*1+8*4 & 2*6+8*9 & 2*2+8*8 \\\ 3*1+5*4 & 3*6+5*9 & 3*2+5*8 \\\ 4*1+7*4 & 4*6+7*9 & 4*2+7*8
\end{bmatrix} = \begin{bmatrix}
34 & 84 & 68 \\\ 23 & 63 & 46 \\\ 32 & 87 & 64
\end{bmatrix}
$$
As you can see, the result is a 3x3 matrix. The size of a product matrix will be the number of rows in A x the number of columns in b
