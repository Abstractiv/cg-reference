---
title: Cloning the Main Site Repo
---

```
git clone --recurse-submodules <repo>
```

```
git submodule update --remote <submodule>
```

You could also just 
```
git clone <repo>
git submodule init
git submodule update
```

Tags: #Git