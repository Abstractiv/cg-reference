---
title: Useful Functions
draft: yes
---
**ofSetBackgroundAuto()**

### Math Functions
**ofDegToRad()**
Should be straightforward, this converts an input in degrees to radians.

**ofMap()**
`ofMap(input, inputmin, inputmax, outputmin, outputmax)`
Works just like the fit() function in VEX.

---
