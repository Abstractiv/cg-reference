### Time
**Seconds**
Getting the time in seconds that TouchDesigner has been opened:
`absTime.seconds`
Good for offsetting noise.

**Frame**
Same story as above, but in number of frames:
`absTime.frame`
