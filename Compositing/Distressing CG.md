From a Reddit post on March 8, 2024 - "Compers - what are you favorite special sauce tricks for distressing CG?"

- Blur it, glow it, ship it.
- BMD Fusion -> jpeg damage node
- Nuke -> Convolve node with an image from an f22 lens.
- Luma key to add heavier noise in the blacks to make it seem like the camera sensor is bad in low light.
- Chromatic Aberration, and perhaps lens distortion depending on how wide the shot is.
- Auto-exposure replication or otherwise animating the exposure level.
- "If you're a wild dude you could even add internal ND filter adjustments over your frames to really sell how shitty of a camera man you are."
- A little bit of blur on the highlights in the blue channel, added on top for those bright pings.
- A very small soften.